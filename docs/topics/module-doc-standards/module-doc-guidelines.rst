====================================
Salt module documentation guidelines
====================================

This document will be a slightly more in-depth guide that explains how to use
the template and what decisions or considerations to keep in mind when writing
module documentation.

It should include a link to the other three guides:

* The template itself
* The contributing guidelines for writing module docs
* The module documentation navigation guidelines

One thing that is going to be really tricky about this guide is how it will
relate to the main Salt Contributing guide. Should this be a link away from that
guide? Should it be included as a section in that guide?

I decided to punt that question for now because I need to just see how long the
final guidelines will be. I also just need to think through general questions
such as whether to have a separate docs contributing guide in general.
