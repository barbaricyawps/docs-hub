.. _branch-versioning:

================================
Multi-version branching strategy
================================

The Salt Docs team is gradually rolling out multi-version support for all the
Salt docs that use the Furo theme. Users can select the version of Salt they
want to read the docs for from a menu in the bottom left menu bar.

Not all Salt documentation has multi-version support yet. The doc sets that have
multi-version support are:

* `Salt install guide <https://docs.saltproject.io/salt/install-guide/en/latest/>`_

This document explains our process for maintaining different versioned branches
of the docs, using the Salt install guide as the example.


Doc changes for a future release
================================
To make documentation changes for a future release, merge all changes into the
``main`` branch. The changes are not published on the live site, but they are
available in the GitLab Pages preview for the site. For example, the Salt
Install Guide is at:

https://saltstack.gitlab.io/open/docs/salt-install-guide/en/latest/index.html


Doc changes for a past release
==============================
The Salt install guide has a branch for each currently supported version:

* ``v3005``
* ``v3004``

To make changes to any branch for a past version:

#. Build a new branch of the upstream version. For example:

   .. code-block:: bash

       git checkout -b new-branch-name upstream/v3005

#. Make the necessary doc changes, commit them, and push them to your personal
   fork.

#. When you open the merge request, do not merge the changes into the ``main``
   branch. Merge the changes into the branch for target version, such as the
   ``v3005`` branch if you were making changes to the 3005 version of the docs.

   .. Warning::
       If you need to make changes to all versions, you will need to open
       a merge request for each branch and duplicate the changes in each
       version.
